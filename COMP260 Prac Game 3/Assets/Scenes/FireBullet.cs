﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {
	public float FireRate = 0.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	public BulletMove bulletPrefab;

	private float counter = -1;


	void Update () {
		if (Time.timeScale == 0) {
			return;
		}

		counter = counter - Time.deltaTime;
		// when the button is pushed, fire a bullet
		if (Input.GetButtonDown("Fire1") && counter < 0) {
			counter = 1/FireRate;
			BulletMove bullet = Instantiate(bulletPrefab);
			// the bullet starts at the player's position
			bullet.transform.position = transform.position;

			// create a ray towards the mouse location
			Ray ray = 
				Camera.main.ScreenPointToRay(Input.mousePosition);
			bullet.direction = ray.direction;
		}
	}


}
