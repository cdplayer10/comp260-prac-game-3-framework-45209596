﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ShellMenu : MonoBehaviour {
	public AudioSource Background;
	public GameObject shellPanel;
	public GameObject optionsPanel;
	public Dropdown qualityDropdown;
	public Dropdown resolutionDropdown;
	public Toggle fullScreenToggle;
	public Slider volumeSlider;	
	public Slider musicSlider;	
	private bool paused = true;

	void Start () {
		SetPaused(paused);
		optionsPanel.SetActive(false);
		qualityDropdown.ClearOptions();
		List<string> names = new List<string>();
		for (int i = 0; i < QualitySettings.names.Length; i++) {
			names.Add(QualitySettings.names[i]);
		}
		qualityDropdown.AddOptions(names);
		resolutionDropdown.ClearOptions();
		List<string> resolutions = new List<string>();
		for (int i = 0; i < Screen.resolutions.Length; i++) {
			resolutions.Add(Screen.resolutions[i].ToString());
		}
		resolutionDropdown.AddOptions(resolutions);
		if (PlayerPrefs.HasKey("AudioVolume")) {
			AudioListener.volume = 
				PlayerPrefs.GetFloat("AudioVolume");
		} else {
			// first time the game is run, use the default value
			AudioListener.volume = 1;
		}
		if (PlayerPrefs.HasKey("BackgroundMusic")) {
			Background.volume = 
				PlayerPrefs.GetFloat("BackgroundMusic");
		} else {
			// first time the game is run, use the default value
			Background.volume = 1;
		}


	}
	public void OnPressedOptions() {
		// show the options panel & hide the shell panel
		shellPanel.SetActive(false);
		optionsPanel.SetActive(true);
		qualityDropdown.value = QualitySettings.GetQualityLevel();
		int currentResolution = 0;
		for (int i = 0; i < Screen.resolutions.Length; i++) {
			if (Screen.resolutions[i].width == Screen.width &&
				Screen.resolutions[i].height == Screen.height) {
				currentResolution = i;
				break;
			}
		}
		resolutionDropdown.value = currentResolution;
		fullScreenToggle.isOn = Screen.fullScreen;
		volumeSlider.value = AudioListener.volume;
	}
	public void OnPressedCancel() {
		// return to the shell menu
		shellPanel.SetActive(true);
		optionsPanel.SetActive(false);
	}
	public void OnPressedApply() {
		// return to the shell menu
		shellPanel.SetActive(true);
		optionsPanel.SetActive(false);
		QualitySettings.SetQualityLevel(qualityDropdown.value);	
		Resolution res =
			Screen.resolutions[resolutionDropdown.value];
		Screen.SetResolution(res.width, res.height, true);
		Screen.SetResolution(res.width, res.height, 
			fullScreenToggle.isOn);
		AudioListener.volume = volumeSlider.value;
		Background.volume = musicSlider.value;
		PlayerPrefs.SetFloat("AudioVolume", AudioListener.volume);
		PlayerPrefs.SetFloat ("BackgroundVolume", Background.volume);

	}

	void Update () {
		// pause if the player presses escape
		if (!paused && Input.GetKeyDown(KeyCode.Escape)) {
			SetPaused(true);
		}
	}

	private void SetPaused(bool p) {
		// make the shell panel (in)active when (un)paused
		paused = p;
		shellPanel.SetActive(paused);
		Time.timeScale = paused ? 0 : 1;
	}
	public void OnPressedPlay() {
		// resume the game
		SetPaused(false);
	}
	public void OnPressedQuit() {
		// quit the game
		Application.Quit();
	}


}
